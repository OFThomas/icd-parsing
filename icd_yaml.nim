import yaml
import streams

import os

type 
  Entry = object
    code: string
    docs: string
    index: string

type 
  Category = object
    category: string
    docs: string
    index: string
    child: seq[Entry]

type 
  CategoryRange = object
    category: string
    docs: string
    index: seq[string]
    child: seq[Category]

type 
  HeadCategory = object
    category: string
    docs: string
    index: seq[string]
    child: seq[CategoryRange]

type 
  ICDCategory = object
    category: string
    docs: string
    index: seq[string]
    child: seq[HeadCategory]

type
  FullCategory = object
    child: seq[ICDCategory]


proc save[T](list: T, path: string) =

  # var entrylist: seq[Entry]
  # entrylist .add Entry(code: "A00.0", docs: "d0", index: "A000")
  # entrylist .add Entry(code: "A00.1", docs: "d1", index: "A001")

  var s = newfilestream(path, fmwrite)
  dump(list, s)
  s.close()
  echo "Written to ", path


proc load[T](path: string): seq[T] =
  var s = newfilestream(path)
  load(s, result)
  echo result
  s.close()
  return result

when isMainModule:
  
  echo "Tests!"
  discard load[Entry]("out.yaml")
  discard load[Entry]("single_cat.yaml")
  var cat_output = load[Category]("category.yaml")

  cat_output.save("cat_out.yaml")
  discard load[CategoryRange]("nested_cat.yaml")

  block nested_cat:
    var res: seq[FullCategory]
    # var res: seq[ICDCategory]
    var s = newfilestream("icd10_arc.yaml")
    load(s, res)
    echo res
    s.close()


  echo "For real"

  var filepath = "out.yaml"
  if paramCount() > 0:
    filepath = paramStr(1)
    echo "Using ", filepath

  # load[Entry](filepath)
  # load[Category](filepath)
  # load[Container](filepath)



