# ICD parsing

To run this script, install R and then install the
tidyverse package by running:

## Installing R

```
sudo apt-get update
sudo apt-get install r-base-core
```

Next you need to export R to your path, add the following to your `.bashrc`
```
export R_HOME=`R RHOME`
```
## Installing the R packages 

Launch the interpreter with `R`, then when you've got the prompt with `>`
on the left hand side:
```r
install.packages("tidyverse")
```

### You'll probably get this error
```
------------------------- ANTICONF ERROR ---------------------------
Configuration failed because libcurl was not found. Try installing:
 * deb: libcurl4-openssl-dev (Debian, Ubuntu, etc)
 * rpm: libcurl-devel (Fedora, CentOS, RHEL)
If libcurl is already installed, check that 'pkg-config' is in your
PATH and PKG_CONFIG_PATH contains a libcurl.pc file. If pkg-config
is unavailable you can set INCLUDE_DIR and LIB_DIR manually via:
R CMD INSTALL --configure-vars='INCLUDE_DIR=... LIB_DIR=...'
--------------------------------------------------------------------
```

To fix, in bash again, you need to exit the R interpreter and run
```
sudo apt-get update
sudo apt-get install libcurl4-openssl-dev -y
```

### Or possibly this error

```
------------------------- ANTICONF ERROR ---------------------------
Configuration failed because libxml-2.0 was not found. Try installing:
 * deb: libxml2-dev (Debian, Ubuntu, etc)
 * rpm: libxml2-devel (Fedora, CentOS, RHEL)
 * csw: libxml2_dev (Solaris)
If libxml-2.0 is already installed, check that 'pkg-config' is in your
PATH and PKG_CONFIG_PATH contains a libxml-2.0.pc file. If pkg-config
is unavailable you can set INCLUDE_DIR and LIB_DIR manually via:
R CMD INSTALL --configure-vars='INCLUDE_DIR=... LIB_DIR=...'
--------------------------------------------------------------------
```
To fix:
```
sudo apt-get update 
sudo apt-get install libxml2-dev
```

### Or maybe this one
```
--------------------------- [ANTICONF] --------------------------------
Configuration failed because openssl was not found. Try installing:
 * deb: libssl-dev (Debian, Ubuntu, etc)
 * rpm: openssl-devel (Fedora, CentOS, RHEL)
 * csw: libssl_dev (Solaris)
 * brew: openssl (Mac OSX)
If openssl is already installed, check that 'pkg-config' is in your
PATH and PKG_CONFIG_PATH contains a openssl.pc file. If pkg-config
is unavailable you can set INCLUDE_DIR and LIB_DIR manually via:
R CMD INSTALL --configure-vars='INCLUDE_DIR=... LIB_DIR=...'
```
To fix 
```
sudo apt-get install libssl-dev
```

Then Launch the `R` interpreter (again!)
```
install.packages("tidyverse")
```

# To install nim dependencies

## To use R and nim

```
nimble install rnim
```

# To use nim to call R scripts

Compile 
```
nim c icd.nim
``` 
Which produces a binary called `icd` which you can run with
```
./icd
```

Or you can compile and run in one line
```
nim c -r icd.nim
```

## To use nim and yaml

```
nimble install yaml
```

# Nim and Yaml parsing

try 
```
nim c -r icd_yaml.nim single_cat.yaml
```

# Notes

in an R console (same as a python session).

The problem: The file icd10_arc.yaml contains a large number of medical
conditions represented by codes that look like I21.0, stored in a defined
hierarchy. The database contains strings like "I210" or " I21.0  ", which
may have leading or trailing whitespace, sometimes missing the dot, and
sometimes with unparsable trailing matter e.g. I2100" or "I210X". The goal
is to write a function which converts this string to a vector of indices.
For example, "A000" corresponds to (1,1,1,1,1) because that is the first
code in the file, and there are 5 levels before getting to A00.0. The
string "I210" gives (1,5,4,2,1) because this is the path down to the code
I21.0.

The function icd10_str_to_indices computes these indices, and also returns
some other information packaged up as a kind of struct (called a named list
in R). The indices item can then be used in icd10_indices_to_code to get
the code that is defined by the indices. Both these functions require the
code definition structure, which is obtained by running icd10_load_codes.
Skip down to the script (search SCRIPT) to see an example.

The issue is that the icd10_str_to_indices function must be called millions
of times, and is quite slow. The slowness is not reading the file from disk
(it is preloaded into memory by icd10_load_codes), but just is rather
caused by the slowness of the search process used to identify the location
of the codes in the file. The search is performed by using "index" keys in
the icd10_arcl.yaml file, which contains either one item (if there is only
one code) or two items (if there is a range of codes). These index keys can
be used as part of a binary search (because icd10_load_file guarantees they
are ordered)

And that is all...


